<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>
		<?php wp_head(); ?>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
            <script src="<?php bloginfo('template_url'); ?>/js/vendor/respond.min.js"></script> <script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>
        <![endif]-->	
        <!-- Start of Async HubSpot Analytics Code -->
		  <script type="text/javascript">
		    (function(d,s,i,r) {
		      if (d.getElementById(i)){return;}
		      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
		      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/412620.js';
		      e.parentNode.insertBefore(n, e);
		    })(document,"script","hs-analytics",300000);
		  </script>
<!-- End of Async HubSpot Analytics Code -->
	</head>
	<body <?php body_class(); ?>>
