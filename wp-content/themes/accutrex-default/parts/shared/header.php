<!--Site Header-->

<div class="site-wrap">
       
        <!--Site Header-->
        <header class="site-header" role="banner">
            <div class="inner-wrap">
                <a href="http://accutrex.com" class="sh-logo-m"><img src="<?php bloginfo('template_url'); ?>/img/logo-mobile.png" alt="Accutrex Logo"></a>
                <div class="site-utility">
                    <span class="sh-icons">
                        <a class="sh-ico-search search-link" target="_blank" href="#"><span>Search</span></a>
                        <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
                    </span>
                    <ul class="sh-secondary-nav clearfix">
                        <li><a href="http://www.accutrex.com/">Home</a></li>
                        <li><a href="http://www.accutrex.com/request-a-quote">Request a Quote</a></li>
                        <li><a href="http://www.accutrex.com/quality">Quality</a></li>
                        <li><a href="http://www.accutrex.com/career-opportunities">Career Opportunities</a></li>
                        <li><a href="http://www.accutrex.com/about_us">About Us</a></li>
                        <li><a href="http://www.accutrex.com/why-accutrex">Why AccuTrex?</a></li>
                        <li><a class="last" href="http://accutrex.com/blog">Blog</a></li>
                    </ul>
                    <div class="sh-contact">
                        <div class="sh-social-group">
                            <a class="sh-social-ico" href="https://www.facebook.com/pages/AccuTrex-Products-Inc/478575812286792?ref_type=bookmark"><img src="<?php bloginfo('template_url'); ?>/img/facebook-22.png" alt=""></a>
                            <a class="sh-social-ico" href="https://www.linkedin.com/company/accutrex-products-inc.?trk=biz-companies-cym"><img src="<?php bloginfo('template_url'); ?>/img/linkedin-22.png" alt=""></a>
                            <a class="sh-social-ico" href="https://twitter.com/Accutrex_Shims"><img src="<?php bloginfo('template_url'); ?>/img/twitter-22.png" alt=""></a>
                            <a class="sh-social-ico" href="https://plus.google.com/113374522159008653939/about"><img src="<?php bloginfo('template_url'); ?>/img/google-plus-22.png" alt=""></a>
                        </div>
                       
                        <span class="sh-ph">Call (724) 746-4300</span>
                    </div>
                </div>
                    <!--Site Nav-->
                    <div class="site-nav-container">
                        <a href="http://accutrex.com" class="sh-logo"><img src="<?php bloginfo('template_url'); ?>/img/accutrex-logo.jpg" alt="Accutrex Logo"></a>
                    <div class="snc-header">
                    <a href="" class="close-menu menu-link">Close</a>
                    </div>
                    <?php wp_nav_menu(array(
                    'menu'            => 'Primary Nav',
                    'container'       => 'nav',
                    'container_class' => 'site-nav',
                    'menu_class'      => 'sn-level-1',
                    'walker'        => new themeslug_walker_nav_menu
                    )); ?>
                     <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module') ); ?>
                    </div>
                    <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
                    <div style="clear:both"></div>
                    <!--Site Nav END-->
            </div>
        </header>

<?php if ( is_front_page()  ) : ?>	   


		<!-- <section class="site-intro-module">
                <div class="inner-wrap">
                    <div class="sim-text">
                        <h1>
                            <span>AccuTrex Products, Inc. Is Proud To Offer</span> Deacon High Temperature Sealants.
                        </h1>
                    </div>
                </div>
            </section> -->
            
            
            
       <?php else : ?>  
<!-- <div class="site-intro-bg">
            <div class="inner-wrap"></div>
        </div> -->
        <?php endif; ?>
   
<!--Site Content-->
<section class="site-content" role="main">