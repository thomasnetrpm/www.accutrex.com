<!--Secondary Content-->
<aside class="site-content-secondary col-3 last">
<h4>Recent Posts</h4>
<ul>
<?php wp_get_archives('type=postbypost&limit=15'); ?>
</ul>
<h4><?php _e('Archives:'); ?></h4>
<ul>
<?php wp_get_archives('type=monthly'); ?>
</ul>
<h4><?php _e('Categories:'); ?></h4>
<ul>
<?php wp_list_cats(); ?>
</ul>


</aside>



