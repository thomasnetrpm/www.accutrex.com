<!--Secondary Content-->
<aside class="site-content-secondary">

	   <h3 class="aside-title">Deacon Sealants</h3>
       <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Side Menu',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>


<?php if(get_field('aside_cta') ): ?>
<div class="cta-aside">
<?php the_field('aside_cta'); ?>
</div>                 
<?php elseif(get_field('global_aside_cta','option') ): ?>
<div class="cta-aside">
<?php the_field('global_aside_cta','option'); ?>
</div>
<?php endif; ?>

    
<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
<?php endif; ?>
</aside>


