</section>



<!--Site Footer-->
        <footer class="site-footer" role="contentinfo">
    <div class="inner-wrap">
        
        <div class="sf-address">
<p>
<b>AccuTrex Products, Inc.</b><br>
<b>Headquarters Division</b><br>
112 Southpointe Blvd.<br>
Canonsburg, PA 15317<br>
Ph: (724) 746-4300<br>
Fx: (724) 746-0711<br>
Fx: (724) 746-0719
</p>

<p><b>Precision Parts Division</b><br>
1968 Sunshine Rd. P.O. Box 789<br>
Lancaster, SC 29721<br>
Ph: (803) 286-4474<br>
Fx: (803) 286-9537</p>
        </div>

        <div class="sf-nav">
            <span id="hs_cos_wrapper_footer_nav" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_menu" style="" data-hs-cos-general-type="widget" data-hs-cos-type="menu"><div id="hs_menu_wrapper_footer_nav" class="hs-menu-wrapper active-branch no-flyouts hs-menu-flow-horizontal" data-sitemap-name="Footer">
 <ul>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/products">Products</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/industries-served">Industries</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/capabilities">Capabilities</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/delivery-services">Services</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/about_us">About Us</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/quality">Quality</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/career-opportunities">Careers</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/contact-us">Contact Us</a></li>
  <li class="hs-menu-item hs-menu-depth-1"><a href="http://www.accutrex.com/site_map">Sitemap</a></li>
 </ul>
</div></span>
            <p>© 2015 AccuTrex Products, Inc.</p>
        </div>

    </div>
</footer>
</div> <!--site-wrap END -->