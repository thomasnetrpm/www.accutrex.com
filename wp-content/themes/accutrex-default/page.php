<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
		
	    <div class="inner-wrap">

	        <article class="site-content-primary"> 
	        	<h1><?php the_title(); ?></h1>
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?>                    
	        </article>
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar' ) ); ?>

			

		</div>


<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
		
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>