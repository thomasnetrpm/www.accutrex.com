<?php
/*
 * Import / Export Cart Based Shipping Settings
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class BEExport_WooCartShipping {

    private $redirect_url;
    private $settings;
 
    /**
     * Initialize a new instance of the Exporter
     */
    function __construct( $cartBasedClass ) {
        // Setup processing requests
        //add_action( 'admin_xml_ns', array( &$this, 'process_settings_export' ), 9999 );
        //add_action( 'wp', array( &$this, 'process_settings_import' ) );

        $this->redirect_url = admin_url( 'admin.php?page=wc-settings&tab=shipping&section=wc_cart_based_shipping' );

        $this->cartBasedClass = $cartBasedClass;

        $this->settings = $cartBasedClass->settings;
        $this->cart_rate_sub_option = $cartBasedClass->cart_rate_sub_option;
        $this->cart_rate_count_option = $cartBasedClass->cart_rate_count_option;
        $this->cart_rate_weight_option = $cartBasedClass->cart_rate_weight_option;
        $this->class_exclusions_options = $cartBasedClass->class_exclusions_options;

        if( isset( $_POST[ 'be-export-cartbased' ] ) ) $this->process_settings_export();
        if( isset( $_POST[ 'be-import-cartbased' ] ) ) $this->process_settings_import();

    }


    /**
     * Print Exporter Settings
     *
     * @param $transient
     * @return void
     */
    function print_exporter_settings () {
?>
    <div class="metabox-holder" style="clear:both; display: block; margin: 0; padding: 0;">
        <div class="postbox" style="width: 49%; float: left;">
            <h3><span><?php _e( 'Export Settings' ); ?></span></h3>
            <div class="inside">
                <form method="post">
                    <p><?php _e( 'Generate backup file to save your settings for this shipping method.', 'be-cart-based' ); ?></p>
                    <p>&nbsp;</p>
                    <p>
                        <?php wp_nonce_field( 'be_cartship_export_nonce', 'be_cartship_export_nonce' ); ?>
                        <?php submit_button( __( 'Export' ), 'secondary', 'be-export-cartbased', false ); ?>
                    </p>
                </form>
            </div><!-- .inside -->
        </div><!-- .postbox -->

        <div class="postbox" style="width: 49%; float: right;">
            <h3><span><?php _e( 'Import Settings' ); ?></span></h3>
            <div class="inside">
                <form method="post" enctype="multipart/form-data">
                    <?php wp_nonce_field( 'woocommerce-settings' ); ?>
                    <p><?php _e( 'Import settings for this method from your saved backup file.', 'be-cart-based' ); ?></p>
                    <p>&nbsp;</p>
                    <p>
                        <input type="file" name="import_file"/>
                        <?php submit_button( __( 'Import' ), 'secondary', 'be-import-cartbased', false ); ?>
                    </p>
                </form>
            </div><!-- .inside -->
        </div><!-- .postbox -->
    </div><!-- .metabox-holder -->
<?php
    }


    /**
     * Generate JSON code on export
     */
    function process_settings_export() {
        if( ! current_user_can( 'manage_options' ) )
            return;

        $settings = $this->process_settings();

        ignore_user_abort( true );

        nocache_headers();
        header( 'Content-Type: application/json; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename=be-cartbased-settings.' . date( 'm-d-Y' ) . '.json' );
        header( "Expires: 0" );

        echo json_encode( $settings );
        exit;
    }


    /**
     * Process JSON code on import
     */
    function process_settings_import() {

        if( ! current_user_can( 'manage_options' ) )
            return;

        // check that a file has been submitted
        $import_file = $_FILES['import_file']['tmp_name'];
        if( empty( $import_file ) )
            return __( 'Please upload a file to import' );

        // check that a JSON file has been uploaded
        $file_name = explode( '.', $_FILES['import_file']['name'] );
        $extension = end( $file_name );
        if( $extension != 'json' )
            return __( 'Please upload a valid .json file' );

        // Retrieve the settings from the file and convert the json object to an array.
        $settings = (array) json_decode( file_get_contents( $import_file ), true );

        $this->restore_settings( $settings );

        return "success";
        //wp_safe_redirect( $this->redirect_url . '&import=success' ); exit;

    }


    /**
     * Combine settings into single array
     *
     * @return array
     */
    function process_settings() {
        // retrieve settings from DB
        $settings = array();

        $settings[ 'cart_rate_shipping' ] = $this->settings;

        if( $this->cart_rate_sub_option != '' )
            $settings[ 'cart_rate_sub_option' ] = get_option( $this->cart_rate_sub_option );

        if( $this->cart_rate_count_option != '' )
            $settings[ 'cart_rate_count_option' ] = get_option( $this->cart_rate_count_option );

        if( $this->cart_rate_weight_option != '' )
            $settings[ 'cart_rate_weight_option' ] = get_option( $this->cart_rate_weight_option );

        if( $this->class_exclusions_options != '' )
            $settings[ 'class_exclusions_options' ] = get_option( $this->class_exclusions_options );

        return $settings;
    }


    /**
     * Combine settings into single array
     *
     * @return array
     */
    function restore_settings( $settings ) {
        $cartBasedClass = $this->cartBasedClass;

        if( !function_exists( 'clean_nested_array' ) ) :
            function clean_nested_array( &$item, $key ) {
                if( is_array( $item ) )
                    $item = array_map('woocommerce_clean',$item);
                else
                    $item = woocommerce_clean( $item );
            }
        endif;

        // setup data as POST variables for processing
        foreach( $settings[ 'cart_rate_shipping' ] as $key => $value )
            if( $value == 'yes' )
                $_POST[ $cartBasedClass->plugin_id . $cartBasedClass->id . '_' . $key ] = 1;
            else
                $_POST[ $cartBasedClass->plugin_id . $cartBasedClass->id . '_' . $key ] = $value;

        // check which process have been backed up
        if( isset( $settings[ 'cart_rate_sub_option' ] ) && $this->cart_rate_sub_option != '' ) {
            // cleanup imported values
            array_walk_recursive( $settings[ 'cart_rate_sub_option' ], 'clean_nested_array' );
            update_option( $this->cart_rate_sub_option, $settings[ 'cart_rate_sub_option' ] );
        }

        if( isset( $settings[ 'cart_rate_count_option' ] ) && $this->cart_rate_count_option != '' ) {
            // cleanup imported values
            array_walk_recursive( $settings[ 'cart_rate_count_option' ], 'clean_nested_array' );
            update_option( $this->cart_rate_count_option, $settings[ 'cart_rate_count_option' ] );
        }

        if( isset( $settings[ 'cart_rate_weight_option' ] ) && $this->cart_rate_weight_option != '' ) {
            // cleanup imported values
            array_walk_recursive( $settings[ 'cart_rate_weight_option' ], 'clean_nested_array' );
            update_option( $this->cart_rate_weight_option, $settings[ 'cart_rate_weight_option' ] );
        }

        if( isset( $settings[ 'class_exclusions_options' ] ) && $this->class_exclusions_options != '' ) {
            // cleanup imported values
            array_walk_recursive( $settings[ 'class_exclusions_options' ], 'clean_nested_array' );
            update_option( $this->class_exclusions_options, $settings[ 'class_exclusions_options' ] );
        }

    }

}

?>